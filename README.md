# UUID version 4 generator with clipboard support

1. Install [Deno](https://deno.land/) JavaScript runtime.
2. Run tests:
`deno run -A drakefile.ts test`
3. Start web server:
`deno run -A drakefile.ts server`
4. Start web server for development:
`deno run --watch -A drakefile.ts server`