import { opine, serveStatic } from "https://deno.land/x/opine@1.7.2/mod.ts";
import { renderFile } from "https://deno.land/x/eta@v1.12.3/mod.ts";
import { dirname, join } from "https://deno.land/x/opine@1.7.2/deps.ts";

////////

const app = opine();
const appDir = dirname(import.meta.url);

app.use(serveStatic(join(appDir, "public")));
app.set("views", join(appDir, "views"));
app.engine(".html", renderFile);

////////

app.get("/", (_req, res) => {
  let uuid = crypto.randomUUID();
  res.set("cache-control", "no-store")
    .render("index.html", { uuid });
});

////////

export { app };