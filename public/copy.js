document.addEventListener("DOMContentLoaded", function() {
  if (navigator.clipboard) {
    var copyAnchor = document.getElementById("copy");
    copyAnchor.innerHTML = "Copy\u{1F5CD}";
  
    copyAnchor.addEventListener("click", function(event) {
      event.preventDefault();
  
      var uuidEl = document.getElementById("uuid");
      navigator.clipboard.writeText(uuidEl.innerText).then(function() {
        alert("Copied to clipboard successfully.");
      }, function() {
        alert("Copy to clipboard failed.");
      }); 
    });
  } else {
    console.log("Clipboard functionality is not supported by this web browser.");
  }
});