import { superdeno } from "https://deno.land/x/opine@1.7.2/test/deps.ts";
import { describe, it } from "https://deno.land/x/opine@1.7.2/test/utils.ts";
import { app } from "./app.ts";

const UUID_REGEX = /[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}/;

describe("main route", () => {
  it("response should contain a uuid", async () => {
    await superdeno(app)
      .get("/")
      .expect(200, UUID_REGEX);
  });
});