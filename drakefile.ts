import { desc, run, sh, task } from "https://deno.land/x/drake@v1.5.0/mod.ts";
import { app } from "./app.ts";

desc("Start web server");
task("server", [], function() {
  let port = 3000;
  app.listen({ port });
  console.log(`Opine started on port ${port}`);
});

desc("Run tests");
task("test", [], async function() {
  await sh("deno test -A");
});

run();